import os
import numpy as np
import pandas as pd
import uproot as up

from bokeh.io import output_file, show
from bokeh.models import (BasicTicker, ColorBar, ColumnDataSource,
                          LogColorMapper, PrintfTickFormatter,
                          LogTicker,
                          Range1d)
from bokeh.plotting import figure
from bokeh.sampledata.unemployment1948 import data as testdata
from bokeh.transform import transform
from bokeh.palettes import viridis as _palette
output_file("triggerCellsOccup.html")

class dotDict(dict):
    """dot.notation access to dictionary attributes"""
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

#########################################################################
################### CONFIGURATION PARAMETERS ############################
#########################################################################

NBINSRZ = 40
NBINSPHI = 60

data_path = '~/Downloads/test_triggergeom.root'
myfile = up.open(data_path)
folder = 'hgcaltriggergeomtester'
tree_name = 'TreeTriggerCells'
tree = myfile[ os.path.join(folder, tree_name) ]
#unicodes
UC = dotDict( dict(phi='\u03A6',
                   )
              )

variables = {'zside', 'subdet', 'layer', 'triggercellieta', 'triggercelliphi', 'x', 'y', 'z'}
assert(variables.issubset(tree.keys()))
# 'neighbor_n', 'neighbor_id', 'neighbor_zside', 'neighbor_subdet', 'neighbor_layer', 'neighbor_waferu', 'neighbor_waferv', 'neighbor_cellu', 'neighbor_cellv', 'neighbor_distance', 'c_n', 'c_id', 'c_zside', 'c_subdet', 'c_layer', 'c_waferu', 'c_waferv', 'c_cellu', 'c_cellv', 'c_ieta', 'c_iphi', 'c_x', 'c_y', 'c_z']

#########################################################################
################### DATA ANALYSIS #######################################
#########################################################################

data = tree.arrays(variables, library='pd')
subdetvals  = sorted(data.subdet.unique())
layervals  = sorted(data.layer.unique())
etavals  = sorted(data.triggercellieta.unique())
phivals  = sorted(data.triggercelliphi.unique())

# print('Values of Trigger Subdet ({}): {}'.format(len(subdetvals), subdetvals)) #ECAL (1), HCAL silicon (2) and HCAL scintillator (10)
# print('Values of Layers ({}): {}'.format(len(layervals), layervals))
# print('Values of Trigger Eta ({}): {}'.format(len(etavals), etavals))
# print('Values of Trigger Phi ({}): {}'.format(len(phivals), phivals))

data = data[ data.zside == 1 ] #only look at positive endcap
data['Rz'] = np.sqrt(data.x*data.x + data.y*data.y) / abs(data.z)
rzBinEdges = np.linspace( data['Rz'].min(), data['Rz'].max(), num=NBINSRZ )

rzBinCenters = ['{:.2f}'.format(x) for x in ( rzBinEdges[1:] + rzBinEdges[:-1] ) / 2 ]


data['Rz'] = pd.cut( np.sqrt(data.x*data.x + data.y*data.y) / abs(data.z),
                     bins=rzBinEdges,
                     labels=False )


data.triggercelliphi = pd.cut( data.triggercelliphi,
                               bins=NBINSPHI,
                               labels=False )

group = data.groupby(['Rz', 'triggercelliphi'], as_index=False).count()
group = group.rename(columns={'triggercelliphi': 'phi', 'z': 'nhits'}) #solve ome override issue with 'size'
group = group[['phi', 'nhits', 'Rz']]

#########################################################################
################### PLOTTING ############################################
#########################################################################
source = ColumnDataSource(group)

mypalette = _palette(40)
mapper = LogColorMapper(palette=mypalette, low=group.nhits.min(), high=group.nhits.max())

shifth, shiftv = 1, 1
p = figure(width=1200, height=400, title="Trigger Cells Occupancy",
           x_range=Range1d(group.phi.min()-shifth,
                           group.phi.max()+shifth),
           y_range=Range1d(group.Rz.min()-shiftv,group.Rz.max()+shiftv),
           tools="hover,box_select,box_zoom,reset", x_axis_location='below',
           x_axis_type='linear', y_axis_type='linear',
           )

binwidth=1
p.rect(x='phi', y='Rz',
       source=source,
       width=binwidth, height=binwidth,
       width_units='data', height_units='data',
       line_color='black', fill_color=transform('nhits', mapper)
       )

color_bar = ColorBar(color_mapper=mapper,
                     ticker=LogTicker(desired_num_ticks=len(mypalette)),
                     formatter=PrintfTickFormatter(format="%d")
                     )
p.add_layout(color_bar, 'right')

p.axis.axis_line_color = 'black'
p.axis.major_tick_line_color = 'black'
p.axis.major_label_text_font_size = '10px'
p.axis.major_label_standoff = 2
#p.xaxis.major_label_orientation = 1.0
p.xaxis.axis_label = UC.phi
p.yaxis.axis_label = 'R/z'
p.yaxis.major_label_overrides = {dig: label for dig,label in enumerate(rzBinCenters)}

p.hover.tooltips = [
    ("phi", "@{phi}"),
    ("R/z", "@{Rz}"),
    ("#hits", "@{nhits}"),
]

show(p)
